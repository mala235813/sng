SHELL := /bin/bash

clean:
	git clean -xdf entities sandbox

shellcheck:
	shellcheck *.sh

test_utils: shellcheck clean
	./bash_unit sng-utils-tests.sh

test_views: shellcheck clean
	./bash_unit sng-views-tests.sh

test: test_utils test_views

sandbox: clean
	./bash_unit sng-sandbox.sh
