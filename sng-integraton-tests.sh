#!/usr/bin/env bash
set -euo pipefail

if ! test -d "${SNG_HOME}"; then
  >&2 echo "SNG_HOME is undefined"
  return 1
fi
source "${SNG_HOME}/sng-cli.sh"
