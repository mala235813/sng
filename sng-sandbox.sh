#!/bin/bash

if ! test -d "${SNG_HOME}"; then
  >&2 echo "SNG_HOME is undefined"
  return 1
fi
source "${SNG_HOME}/sng-utils.sh"
source "${SNG_HOME}/sng-views.sh"

test_sandbox() {

  # Members
  local member1="$(cut -f2 < "$(sngu_new_member 110 foo@zoo 'Foo Zoo' | sngu_save_entity member -v)")"
  local member2="$(cut -f2 < "$(sngu_new_member 120 foo@zoo 'Bar Zoo' | sngu_save_entity member -v)")"
  local member3="$(cut -f2 < "$(sngu_new_member 130 foo@zoo 'Qux Zoo' | sngu_save_entity member -v)")"

  # Posts
  local post1="$(head -n1 < "$(sngu_new_post 210 "$member1" <<EOF | sngu_save_entity post -v
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
ut labore et dolore magna aliqua. Velit scelerisque in dictum non consectetur a erat
nam. Massa tincidunt nunc pulvinar sapien.
EOF
  )" | cut -f2)"
  local post2="$(head -n1 < "$(sngu_new_post 220 "$member2" <<EOF | sngu_save_entity post -v
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
ut labore et dolore magna aliqua. Sit amet volutpat consequat mauris nunc. Tristique
senectus et netus et malesuada fames ac turpis. Id consectetur purus ut faucibus pulvinar
elementum integer. Morbi quis commodo odio aenean sed adipiscing diam.
EOF
  )" | cut -f2)"

  # Likes
  sngu_new_like 321 "$member2" "$post1" | sngu_save_entity like
  sngu_new_like 331 "$member3" "$post1" | sngu_save_entity like
  sngu_new_like 332 "$member3" "$post2" | sngu_save_entity like


  # Follows
  sngu_new_follow 421 "$member2" "$member1" | sngu_save_entity follow
  sngu_new_follow 431 "$member3" "$member1" | sngu_save_entity follow
  sngu_new_follow 432 "$member3" "$member2" | sngu_save_entity follow

  echo
  sngu_list '*'
  echo
  sngv_members | sngu_format_table
  echo
  sngv_posts | sngu_format_table
}
	# mkdir sandbox/
	# cd sandbox/
	# mkdir sng-hub.git/
	# pushd sng-hub.git/
	# git init --bare
	# popd
	# mkdir sng-node1
	# pushd sng-node1
	# sng join ../sng-hub.git/
	# popd
	# mkdir sng-node2
	# pushd sng-node2
	# sng join ../sng-hub.git/
	# popd
