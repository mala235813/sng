#!/usr/bin/bash

if ! test -d "${SNG_HOME}"; then
  >&2 echo "SNG_HOME is undefined"
  return 1
fi
source "${SNG_HOME}/sng-utils.sh"
source "${SNG_HOME}/sng-views.sh"

###############################################################################
#                           sng - the main function                           #
###############################################################################
sng() {
  local subcmd=$1
  shift
  local time=$(sngu_new_time)

  # shellcheck disable=SC2048,SC2086
  sngc_"${subcmd}" "$time" $*
}

###############################################################################
#                    sng create - crate a new sng community                   #
###############################################################################
function sngc_create {
  if [ -n "$(ls -A .)" ]; then
    >&2 echo "Current directory must be empty"
    return 1
  fi

  local repo_dir="$(pwd)"
  local tempdir="$(mktemp -d)"

  git init --bare
  pushd "$tempdir" || return 1
  git clone "$repo_dir" repo
  pushd "repo" || return 1
  git checkout --orphan master
  git commit --allow-empty -m "New network"
  popd || return 1
  popd || return 1
  rm -rf "$tempdir"
}

###############################################################################
#                      sng join - join a sng community                      #
###############################################################################
function sngc_join() {
  local time=$1
  local repo=$2
  local name_optional=$3
  local email_optional=$4

  if [ -n "$(ls -A .)" ]; then
    >&2 echo "Current directory must be empty"
    return 1
  fi

  if ! git clone "$repo" . ; then
    >&2 echo "Failed to clone $repo"
    return 1
  fi

  if [ "$name_optional" = "" ] ; then
    local name=$(sngu_get_username)
  else
    local name="$name_optional"
    sngu_set_username "$name"
  fi

  if [ "$email_optional" = "" ] ; then
    local email=$(sngu_get_email)
  else
    local email="$email_optional"
    sngu_set_email "$email"
  fi

  local file="$(sngu_new_member "$time" "$email" "$name" |\
    sngu_save_entity "member" -v)"

  git add "$file"
  git commit -m "joins the network"
}

###############################################################################
#                           sng pull - pull changes                           #
###############################################################################
function sngc_pull() {
  local email="$(sngu_get_email)"
  local member_id="$(sngv_members | grep "$email" | cut -f1)"
  local current_commit="$(git show -q origin/master | head -n1 | cut -d' ' -f2)"
  git fetch --all 1>/dev/null
  git rebase origin/master 1>/dev/null
  sngv_posts_by_followed_members "$member_id" "$current_commit" master |\
    sngu_format_table
}

###############################################################################
#                           sng push - push changes                           #
###############################################################################
function sngc_push() {
  sng pull
  git push
}

###############################################################################
#                        sng members - list all members                       #
###############################################################################
function sngc_members() {
  sngv_members | sngu_format_table
}

###############################################################################
#                          sng posts - list all posts                         #
###############################################################################
function sngc_posts() {
  sngv_posts_with_header | sngu_format_table
}

###############################################################################
#                          sng post - add a new post                          #
###############################################################################
function sngc_post {
  local time=$1
  local email="$(sngu_get_email)"
  local member_id="$(sngv_members | grep "$email" | cut -f1)"
  local file="$(sngu_new_post "$time" "$member_id" | sngu_save_entity post -v)"
  git add "$file"
  git commit -m "adds a post"
}

###############################################################################
#                            sgn show - show a post                           #
###############################################################################
function sngc_show() {
  local post_id=$2

  local message="$(tail -n +2 "$(sngu_entity_file "$post_id" "post")")"
  local line="$(sngv_posts | grep "$post_id")"
  local when="$(echo "$line" | cut -f2)"
  local who="$(echo "$line" | cut -f3)"
  local likes="$(echo "$line" | cut -f4)"
  echo "post id: $post_id"
  echo "when: $when"
  echo "who: $who"
  echo "likes: $likes"
  echo "message:"
  echo "$message"
}


###############################################################################
#            sgn follow - follow a member and see new posts on pull           #
###############################################################################
function sngc_follow() {
  local time=$1
  local followed_member_id=$2
  local email="$(sngu_get_email)"
  local my_member_id="$(sngv_members | grep "$email" | cut -f1)"

  local file="$(sngu_new_follow "$time" "$my_member_id" "$followed_member_id" | sngu_save_entity follow -v)"
  git add "$file"
  git commit -m "follows someone"
}

###############################################################################
#                   sgn unfollow - stop following a member                    #
###############################################################################
function sngc_unfollow() {
  local followed_member_id=$2
  local email="$(sngu_get_email)"
  local my_member_id="$(sngv_members | grep "$email" | cut -f1)"

  local line="$(sngu_list follow | grep "$my_member_id	$followed_member_id")"
  local follow_id="$(echo "$line" | cut -f2)"
  local file="$(sngu_entity_file "$follow_id" follow)"
  git rm -f "$file" 1>/dev/null
  git commit -m "unfollows someone"
}

###############################################################################
#                            sng like - like a post                           #
###############################################################################
function sngc_like() {
  local time=$1
  local post_id=$2
  local email="$(sngu_get_email)"
  local my_member_id="$(sngv_members | grep "$email" | cut -f1)"

  local file="$(sngu_new_like "$time" "$my_member_id" "$post_id" | sngu_save_entity like -v)"
  git add "$file"
  git commit -m "likes a post"
}

###############################################################################
#                       sng log - see who done what when                      #
###############################################################################
function sngc_log() {
  shift
  local args=$*
  # shellcheck disable=SC2086
  git log --oneline --graph --pretty=tformat:$'\t%ad %C(green)%<(7,trunc)%h%C(reset) %C(red)%aN%C(reset) %C(blue)%<(50,trunc)%s%C(reset) %C(auto)%d%C(reset)' --branches --remotes --tags --date=format-local:'%Y-%m-%d %H:%M' --abbrev=4 -20 $args
}
