#!/bin/bash

if ! test -d "${SNG_HOME}"; then
  >&2 echo "SNG_HOME is undefined"
  return 1
fi
source "${SNG_HOME}/sng-views.sh"

setup_suite() {
    make sandbox > /dev/null
}

teardown_suite() {
    make clean > /dev/null
}

test_view_posts() {
    assert_equals "4" "$(sngv_posts_with_header | wc -l | sngu_trim)"
}

test_view_members() {
    assert_equals "5" "$(sngv_members | wc -l | sngu_trim)"
}
