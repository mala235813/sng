#!/bin/bash

if ! test -d "${SNG_HOME}"; then
  >&2 echo "SNG_HOME is undefined"
  return 1
fi
source "${SNG_HOME}/sng-utils.sh"

test_new_time() {
  local time1=$(sngu_new_time)
  local time2=$(sngu_new_time)
  sleep 1
  local time3=$(sngu_new_time)

  assert "test $time1 -le $time2" "time1 <= time2 failed"
  assert "test $time2 -lt $time3" "time2 < time3 failed"
}

test_save_entity() {
  local data='1234ABCD4321	aaa	bbb'
  local entity="foo"

  local path=$(echo "$data" | sngu_save_entity "$entity" -v)

  assert_equals "$entity" "$(basename "$path")"
  assert "test -e $path"
  assert_equals "[$entity]	$data" "$(cat "$path")"

  rm "$path"
}

test_new_member() {
  local line=$(sngu_new_member 123456 'foo@bar.com' 'foo bar')

  assert_equals "$(sngu_persistent_id 'foo@bar.com	foo bar')" "$(echo "$line" | cut -f1)"
  assert_equals "123456" "$(echo "$line" | cut -f2)"
  assert_equals "foo@bar.com" "$(echo "$line" | cut -f3)"
  assert_equals "foo bar" "$(echo "$line" | cut -f4)"
}

test_trim_start() {
  assert_equals "foo" "$(echo "foo" | sngu_trim_start)"
  assert_equals "foo" "$(echo "      foo" | sngu_trim_start)"
  assert_equals "foo" "$(echo "	foo" | sngu_trim_start)"
  assert_equals "foo" "$(echo "	   	foo" | sngu_trim_start)"
  assert_equals "foo    " "$(echo "foo    " | sngu_trim_start)"
  assert_equals "foo	  	" "$(echo "      foo	  	" | sngu_trim_start)"
}

test_trim_end() {
  assert_equals "foo" "$(echo "foo" | sngu_trim_end)"
  assert_equals "      foo" "$(echo "      foo" | sngu_trim_end)"
  assert_equals "	foo" "$(echo "	foo" | sngu_trim_end)"
  assert_equals "foo" "$(echo "foo    " | sngu_trim_end)"
  assert_equals "  foo" "$(echo "  foo	  	" | sngu_trim_end)"
}

test_trim() {
  assert_equals "foo	 bar" "$(echo "  	   		 foo	 bar	   	 		" | sngu_trim)"
}

test_new_post() {
  local time=222111
  local member_id=1234

  local post="$(sngu_new_post "$time" "$member_id" <<EOF
first line
second line
third line
EOF
)"
  assert_equals "4" "$(echo "$post" | wc -l | sngu_trim)"
}

test_new_like() {
  local time=222111
  local member_id=1234
  local post_id=6543

  local line="$(sngu_new_like "$time" "$member_id" "$post_id")"
  assert_equals "$(sngu_persistent_id "$time	$member_id	$post_id")" "$(echo "$line" | cut -f1)"
  assert_equals "$time" "$(echo "$line" | cut -f2)"
  assert_equals "$member_id" "$(echo "$line" | cut -f3)"
  assert_equals "$post_id" "$(echo "$line" | cut -f4)"
}

test_new_follow() {
  local time=222111
  local member_id=1234
  local followed_member_id=6543

  local line="$(sngu_new_like "$time" "$member_id" "$followed_member_id")"
  assert_equals "$(sngu_persistent_id "$time	$member_id	$followed_member_id")" "$(echo "$line" | cut -f1)"
  assert_equals "$time" "$(echo "$line" | cut -f2)"
  assert_equals "$member_id" "$(echo "$line" | cut -f3)"
  assert_equals "$followed_member_id" "$(echo "$line" | cut -f4)"
}

test_list() {
  sngu_new_member 123 foo@zoo.com 'Foo Zoo' | sngu_save_entity member
  sngu_new_member 234 quz@zoo.com 'Qux Zoo' | sngu_save_entity member
  sngu_new_member 434 quuz@zoo.com 'Quux Zoo' | sngu_save_entity member

  assert_equals "3" "$(sngu_list member | wc -l | sngu_trim)"
}
