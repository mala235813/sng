# sng

A social network based on git
After installation, please join the sgn community at https://gitlab.com/mala235813/sng-hub

## Installation
In order to use sng do the following
 1. Make sure that shellcheck is installed.
 2. Make sure that md5sum is installed.
 3. Clone `https://gitlab.com/mala235813/sng.git` to any directory, from now on called `<sng-dir>`.
 4. Run ```cd <sng-dir>```
 5. Run ```make test```. All should be OK.
 6. Add these 2 lines to your .bashrc file
 ```
export SNG_HOME=<sng-dir>
source ${SNG_HOME}/sng-cli.sh 
 ``` 
 7. Restart your bash shell 
## Usage
Here follows a description of the comand line interface.

|command| summary |
| ------- | ----- |
|sng create | crate a new sng community|
|sng join | join a sng community|
|sng pull | pull changes|
|sng push | push changes|
|sng members | list all members|
|sng posts | list all posts|
|sng post | add a new post|
|sgn show | show a post|
|sgn follow | follow a member and see new posts on pull|
|sgn unfollow | stop following a member|
|sng like | like a post|
|sng log | see who done what when|

### sng create
The `sng create` command creates a new bare empty git repository. It must be run in an empty directory, or it will fail.
Using `sng create` is optional. You can as well create an new online repository, on gitlab or github for example.

### sng join
The `sng join` command joins a sng community by cloning it.
Usage:

``` sh
sng join <repo> <name> <email>
  # required <repo> is the url or local path to an sng community repo.
  # optional <name> is your full name. If not provided your existing git user name is used
  # optional <email> is your email. If not provided your existing git email is used
```
### sng pull
The `sng pull` command pulls activities (joins, likes, posts, follows) that other members have pushed to the community repo.
If you have unpublished activities, they will be rebased on the pulled activities.
If you are following other members, new posts from them will be listed.

### sng push
The `sng push` command pushes your activities (joins, likes, posts, follows) to the community repo.
A `sng pull` is automatically done before the push.

### sng members
The `nsg members` command lists all members

### sng posts
The `nsg posts` command lists all posts

### sng post
The `sng post` command adds a new post, by reading text from stdin.
Usage:
``` sh
sng post <<EOF
Some text
some more text
some more text
EOF
```

### sng show
The `sng show` command show full info about a specific post.
Usage:
``` sh
sng show <post_id>
# required <post_id>, use sng posts to see avaliable ids.
```


### sgn follow
Follow a member and see new posts on pull.
Usage:
``` sh
sng follow <member_id>
  # required <member_id>, use sng members to see avaliable ids.
```

### sgn unfollow
Stop following a member.
``` sh
sng unfollow <member_id>
  # required id of a member that you are following <member_id>, use sng members to see avaliable ids.
```
### sng like
Like a post. Likes are shown in `sng posts` and `sgn show`
``` sh
sng like <post_id>
  # required <post_id>, use sng posts to see avaliable ids.
```
### sng log
Show a formatted git log, to see who done what when.
``` sh
sng log <args>
  # optional <args>, these are passed on to git log, to override existing ones.
```
