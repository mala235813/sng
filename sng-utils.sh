#!/usr/bin/bash

sngu_map() {
  local func_and_args="$*"
  while read -r line; do
    echo "$line" | eval "$func_and_args"
  done
}

sngu_format_table() {
  column -s$'\t' -t
}

sngu_conv_time() {
  read -r seconds
  if [ "$(uname)" = "Linux" ]; then
    date -d @"$seconds" "+%Y-%m-%d %H:%M:%S"
  elif [ "$(uname)" = "Darwin" ]; then
    date -r "$seconds" "+%Y-%m-%d %H:%M:%S"
  else
    echo "$seconds"
  fi
}

sngu_header() {
  local column_names="$(echo "$*" | tr ' ' $'\t')"
  echo "$column_names"
  echo "$column_names" | tr 'a-zA-Z0-9' '-'
}

sngu_post_get_id() {
  local line="$1"
  echo "$line" | cut -f2
}

sngu_post_add_likes() {
  read -r line
  local post_id="$(sngu_post_get_id "$line")"
  local likes="$(sngu_list like | grep -c "$post_id" | sngu_trim)"
  echo "$line	$likes"
}

sngu_entity_add_formatted_time() {
  read -r line
  local time="$(echo "$line" | cut -f3)"
  local formatted_time="$(echo "$time" | sngu_conv_time)"
  echo "$line	$formatted_time"
}
sngu_post_add_summary() {
  local length=$1
  read -r line
  local post_id="$(sngu_post_get_id "$line")"
  local summary="$(sngu_post_summary "$post_id" "$length")"
  echo "$line	$summary"
}

sngu_trim_start() {
  sed -e 's/^[	 ]*//'
}

sngu_trim_end() {
  sed -e 's/[	 ]*$//'
}

sngu_trim() {
  sngu_trim_start | sngu_trim_end
}

sngu_persistent_id() {
  if [[ "$OSTYPE" == "darwin"* ]]; then
    echo "$1" | md5 | cut -d ' ' -f 1 | tr '[:upper:]' '[:lower:]'
  else
    echo "$1" | md5sum | cut -d ' ' -f 1 | tr '[:upper:]' '[:lower:]'
  fi
}

sngu_new_time() {
  date "+%s"
}

sngu_post_summary() {
  local id=$1
  local length=$2
  local file="$(sngu_entity_file "$id" "post")"
  echo "$(sed -n 2p "$file" | cut -c1-"$length" | sngu_trim) ..."
}

sngu_entity_dir() {
  local entity_id=$1
  # shellcheck disable=SC2001
  echo "entities/$(echo "$entity_id" | sed 's_\(....\)_&/_g')"
}

sngu_entity_file() {
  local entity_id=$1
  local entity_type=$2
  echo "$(sngu_entity_dir "$entity_id")$entity_type"
}

sngu_read_multiline() {
  while read -r line; do
    echo "$line"
  done
  if [ "${line}" != "" ]; then
    echo "${line}"
  fi
}

sngu_get_username() {
  git config --get user.name
}

sngu_set_username() {
  local name="$1"
  git config user.name "$name"
}

sngu_get_email() {
  git config user.email
}

sngu_set_email() {
  local email="$1"
  git config user.email "$email"
}

sngu_save_entity() {
  local entity_type="$1"
  local verbose="$2"
  local line
  read -r line
  local id=$(echo "$line" | cut -f1)
  local dir="$(sngu_entity_dir "$id")"
  local file="${dir}${entity_type}"

  mkdir -p "$dir"
  echo "[${entity_type}]	$line" > "$file"

  sngu_read_multiline >> "$file"

  if [ "$verbose" = "-v" ]; then
    echo "$file"
  fi
}

sngu_new_member() {
  local time="$1"
  local email="$2"
  local name="$3"
  local data="${email}	${name}"
  local id=$(sngu_persistent_id "${data}")
  echo "${id}	${time}	${data}"
}

sngu_new_post() {
  local time=$1
  local member_id=$2
  local message=$(sngu_read_multiline)
  local data=$(echo "$time	$member_id"; echo "$message")
  local id=$(sngu_persistent_id "$data")
  echo "${id}	${data}"
}

sngu_new_like() {
  local time=$1
  local member_id=$2
  local post_id=$3
  local data="$time	$member_id	$post_id"
  local id=$(sngu_persistent_id "$data")
  echo "${id}	${data}"
}

sngu_new_follow() {
  local time=$1
  local member_id=$2
  local followed_member_id=$3
  local data="$member_id	$followed_member_id"
  local id=$(sngu_persistent_id "$data")
  echo "${id}	${time}	${data}"
}

sngu_list() {
  local entity_type=$1
  if [ "$entity_type" = "" ]; then
    find entities/ -type f
  else
    find entities/ -type f -name "$entity_type"
  fi | xargs cat | grep '^\[' | sort -t$'\t' -k3n
}
