#!/usr/bin/bash

if ! test -d "${SNG_HOME}"; then
  >&2 echo "SNG_HOME is undefined"
  return 1
fi
source "${SNG_HOME}/sng-utils.sh"

sngv_members() {
  sngu_header "member-id when email name"
  sngu_list member |\
    sngu_map sngu_entity_add_formatted_time |\
    sort -k3n |\
    awk 'BEGIN { FS="\t"; OFS=FS } { print $2, $6, $4, $5 }'
}

sngv_posts() {
  local posts="$( \
    sngu_list post |\
    sngu_map sngu_entity_add_formatted_time |\
    sngu_map sngu_post_add_summary 20 |\
    sngu_map sngu_post_add_likes |\
    sort -k4)"
  local members="$( \
    sngu_list member |\
    sort -k2)"

  # echo "$posts"
  # echo "$members"
  join -t$'\t' -1 4 -2 2 <(echo "$posts") <(echo "$members") |\
    sort -k4nr |\
    awk 'BEGIN { FS="\t"; OFS=FS } { print $3, $5, $11, $7, $6 }'
}

sngv_posts_header() {
  sngu_header "post-id when who likes what"
}

sngv_posts_with_header() {
  sngv_posts_header
  sngv_posts
}

sngv_posts_by_followed_members() {
  local follower=$1
  local from_commit=$2
  local to_commit=$3
  local followed_member_ids=$(sngu_list "follow" |\
    while read -r line; do
      if [ "$follower" = "$(echo "$line" | cut -f 4)" ]; then
        echo "$line" | cut -f 5
      fi
    done)

  local all_posts_in_interval="$(git diff --stat "$from_commit" "$to_commit"  |\
    sngu_trim_start |\
    cut -d' ' -f1 |\
    grep 'post$' |\
    xargs cat |\
    grep '^\[post\]')"
  local followed_posts="$(join -t$'\t' -1 1 -2 4 <(echo "$followed_member_ids" | sort -k 1) <(echo "$all_posts_in_interval" | sort -k 4))"
  local followed_post_ids="$(echo "$followed_posts" | cut -f 3)"
  sngv_posts_header
  join -t$'\t' -1 1 -2 1 <(echo "$followed_post_ids" | sort -k 1) <(sngv_posts | sort -k1) | sort -k2r
}
